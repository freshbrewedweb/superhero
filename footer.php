<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package _mbbasetheme
 */
?>

	</div><!-- #content -->

	<!-- <div class="instagram-feed">

	</div> -->
	<footer id="colophon" class="site-footer" role="contentinfo">
	  <div class="wrap">
			<div class="footer-cells">
  		<?php if ( ! dynamic_sidebar( 'footer' ) ) : ?>

  			<aside id="search" class="foot-cell widget_search">
  				<?php get_search_form(); ?>
  			</aside>

  			<aside id="archives" class="foot-cell">
  				<h1 class="foot-cell-title"><?php _e( 'Archives', '_mbbasetheme' ); ?></h1>
  				<ul>
  					<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
  				</ul>
  			</aside>

  			<aside id="meta" class="foot-cell">
  				<h1 class="foot-cell-title"><?php _e( 'Meta', '_mbbasetheme' ); ?></h1>
  				<ul>
  					<?php wp_register(); ?>
  					<li><?php wp_loginout(); ?></li>
  					<?php wp_meta(); ?>
  				</ul>
  			</aside>

  		<?php endif; // end sidebar widget area ?>
	    </div>
	  </div>
		<div class="site-info">
			<div class="wrap">
				<?php wp_nav_menu(array("theme_location" => "footer")); ?>
				<div class="copyright">&copy; <?php echo date( "Y" ); echo " "; bloginfo( 'name' ); echo " "; echo ubiweb_copyright(); ?></div>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php get_sidebar("kick") ?>

<?php wp_footer(); ?>

</body>
</html>
