<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package _mbbasetheme
 */
?>
<?php

  $logo = get_option('sitelogo');
  $logo_src = $logo["upload"];

  $header = get_option('title_tagline');

?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/apple-touch-icon.png">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="fb-root"></div>
<div id="page" class="hfeed site">
	<!--[if lt IE 9]>
	    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->

	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', '_mbbasetheme' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
	  <div class="wrap">
  		<div class="site-branding">
  			<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
  			  <?php
  			  if( $logo_src ) {
    			  echo '<img src="'.$logo_src.'" alt="'. get_bloginfo( 'name' ) .'">';
  			  } else {
    			  bloginfo( 'name' );
  			  }
  			  ?>
  			 </a></h1>
  			<?php if(get_bloginfo( 'description' ) != ""): ?>
  			<h2 class="site-description"><?php bloginfo( 'description' ); ?></h2>
  			<?php endif; ?>
  		</div>

  		<nav id="site-navigation" class="main-navigation" role="navigation">
  			<!-- <button class="menu-toggle"><?php _e( 'Primary Menu', '_mbbasetheme' ); ?></button> -->
  			<button class="js-toggle-kick"><i class="fa fa-<?php if($header['icon']) echo $header['icon']; else echo "send"; ?>"></i></button>
  			<?php wp_nav_menu( array( 'theme_location' => 'primary', 'container_class' => 'menu-primary' ) ); ?>
  		</nav><!-- #site-navigation -->
	  </div><!-- .wrap -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
