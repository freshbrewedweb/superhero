<?php
/**
 * Template Name: Landing Page
 *
 *
 * @package _mbbasetheme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'fullpage' ); ?>

				<?php if( have_rows('section') ): ?>
				    <?php while ( have_rows('section') ) : the_row(); ?>

				        <?php
								//ICON SECTION
								if( get_row_layout() == 'icon_section' ):
								?>
								<?php $color = get_sub_field('color') ?>
								<?php $bg_color = get_sub_field('background_color') ?>
								<?php $bg_image = get_sub_field('background_image') ?>
								<section class="section-icon section-icon--<?= strtolower($color) ?>" style="<?= ($bg_color) ? 'background-color:' . $bg_color . ';' : NULL ?><?= ($bg_image) ? 'background-image: url(' . $bg_image['sizes']['full-page'] . ');' : NULL ?>">
									<div class="wrap contain">
										<h1 class="lined-title lined-title--<?= strtolower($color) ?>"><span><?php the_sub_field('title') ?></span></h1>
										<div class="text-upper"><?php the_sub_field('description') ?></div>
					        	<?php if( have_rows('icons') ): ?>
								 		<ul class="section-icon__list">
								    <?php while ( have_rows('icons') ) : the_row(); ?>
											<?php $image = get_sub_field('icon'); ?>
											<li>
												<img src="<?=  $image['url'] ?>" alt="<?= $image['alt'] ?>">
												<h3><?php the_sub_field('title') ?></h3>
												<p><?php the_sub_field('description') ?></p>
											</li>
										<?php endwhile; ?>
										</ul>
										<?php endif; ?>
									</div>
								</section><!-- .section-icon -->
				        <?php endif; //layout ?>


								<?php
								//IMAGE PANEL SECTION
								if( get_row_layout() == 'image_panel' ):
								?>
								<?php $bg_image = get_sub_field('background_image') ?>
								<section class="image-panel image-panel--<?= strtolower(get_sub_field('position')) ?> image-panel--<?= strtolower(get_sub_field('color')) ?>" style="<?= ($bg_image) ? 'background-image: url(' . $bg_image['sizes']['full-page'] . ');' : NULL ?>">
									<div class="image-panel__content">
										<div><?php the_sub_field('content') ?></div>
									</div>
								</section><!-- .image-panel -->
				        <?php endif; //layout ?>

								<?php
								//HTML Content
								if( get_row_layout() == 'html_content' ):
								?>
								<?php $color = get_sub_field('color') ?>
								<?php $bg_color = get_sub_field('background_color') ?>
								<section class="section-icon section-icon--<?= strtolower($color) ?>" style="<?= ($bg_color) ? 'background-color:' . $bg_color . ';' : NULL ?>">
									<div class="wrap contain">
										<?php the_sub_field('content_area') ?>
									</div>
								</section><!-- .image-panel -->
				        <?php endif; //layout ?>


								<?php
								//CALL TO ACTION SECTION
								if( get_row_layout() == 'cta_section' ):
								?>
								<?php $padding = get_sub_field('padding') ?>
								<?php $color = get_sub_field('color') ?>
								<?php $bg_color = get_sub_field('background_color') ?>
								<?php $bg_image = get_sub_field('background_image') ?>
								<section class="section-cta section-cta--<?= strtolower($color) ?>" style="<?= ($bg_color) ? 'background-color:' . $bg_color . ';' : NULL ?><?= ($bg_image) ? 'background-image: url(' . $bg_image['sizes']['full-page'] . ');' : NULL ?><?= ($padding) ? 'padding-top:'.$padding.'rem;padding-bottom:'.$padding.'rem;' : NULL ?>">
									<div class="wrap">
										<?php if( $title = get_sub_field('title') ): ?>
										<h1><?= $title ?></h1>
										<?php endif; ?>

										<?php the_sub_field('content') ?>
										<?php if( $btnURL = get_sub_field('button_destination') ): ?>
										<a href="<?= $btnURL ?>" class="btn btn-primary btn-lg"><?php the_sub_field('button_text') ?></a>
										<?php endif; ?>
									</div>
								</section><!-- .section-cta -->
				        <?php endif; //layout ?>


				    <?php endwhile; ?>
				<?php endif; //section ?>

			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php get_footer(); ?>
