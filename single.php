<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _mbbasetheme
 */

get_header(); ?>

  <div class="wrap">
  	<div id="primary" class="content-area content-area-sidebar">
  		<main id="main" class="site-main site-main-sidebar" role="main">
  
  		<?php while ( have_posts() ) : the_post(); ?>
  
  			<?php get_template_part( 'content', 'single' ); ?>

  			<?php ubi_author_box(); ?>
  
  			<?php _mbbasetheme_post_nav(); ?>
  
  			<?php
  				// If comments are open or we have at least one comment, load up the comment template
  				if ( comments_open() || '0' != get_comments_number() ) :
  					comments_template();
  				endif;
  			?>
  
  		<?php endwhile; // end of the loop. ?>
  
  		</main><!-- #main -->
  	</div><!-- #primary -->
    <?php get_sidebar(); ?>
  </div><!-- .wrap -->

<?php get_footer(); ?>
