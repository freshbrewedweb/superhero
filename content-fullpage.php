<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _mbbasetheme
 */
?>
<?php

$url = wp_get_attachment_url( get_post_thumbnail_id($post->ID, 'full-page') );

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="page-header<?php echo (($url) ? ' page-header--bg' : NULL) ?>"<?php echo (($url) ? 'style="background-image:url('.$url.')"' : NULL) ?>>
		<?php
		$splash = get_field('splash');
		if( $splash == "Default" || is_null($splash) ) {
			the_title( '<h1 class="entry-title">', '</h1>' );
		} elseif($splash == "Use Content") {
			echo '<div class="text-center">';
			the_content();
			echo "</div>";
		}
		?>
	</header><!-- .entry-header -->
  <div class="wrap">
		<?php
		if( get_field('share_on') == 1 ) {
			echo '<div class="full-page-share-post">';
			ubi_share_post();
			echo '</div>';
		}
		?>
		<?php if( $splash != "Use Content" ): ?>
  	<div class="entry-content">
  		<?php the_content(); ?>
  	</div><!-- .entry-content -->
		<footer class="entry-footer">
  		<?php edit_post_link( __( 'Edit', '_mbbasetheme' ), '<span class="edit-link">', '</span>' ); ?>
  	</footer><!-- .entry-footer -->
		<?php endif; ?>
  </div>
</article><!-- #post-## -->
