(function($) {


    $(".fitvid").fitVids();
/* ==========================================================================
   KICK SIDEBAR
   ========================================================================== */
   function toggleKick(){
     var $kickSidebar = $("#kick-sidebar");
     $kickSidebar.toggleClass("active");
     $(".darken-page").fadeToggle(200);
   }

   $(".js-toggle-kick").click(toggleKick);

   $("#kickClose").click(function(){
     var $kickSidebar = $(this).parent("#kick-sidebar");
     if( $kickSidebar.hasClass("active") ) {
       $kickSidebar.removeClass("active");
       $(".darken-page").fadeOut(200);
     }
   });

   //TRIGGER WAYPOINTS
   $(".share-post").waypoint('sticky', {
       offset: $(".site-header").height()
   });

   $(".toggle-kick").click(function(){
     toggleKick();
   });

  //  $(".author-box").waypoint(function(direction){
  //    if( direction === "down" ) {
  //      toggleKick();
  //    }
  //  }, {
  //    offset: "50%"
  //  });


  /* ==========================================================================
     SECTION NAVIGATION
     ========================================================================== */
/*
  //Nav Scrollspy
  // Cache selectors
  var lastId,
  topMenu = $(".topbar"),
  topMenuHeight = topMenu.outerHeight(),
  // All list items
  menuItems = topMenu.find("a[href^=\"#\"]"),
  // Anchors corresponding to menu items
  scrollItems = menuItems.map(function(){
    var item = $($(this).attr("href"));
    if (item.length) {
      return item;
    }
  });

  if ( !location.hash ) {
    history.pushState({}, "", "#home");
  }

  //Initialize section
  var offsetTop = $(location.hash).offset().top-topMenuHeight+1;
  $("html, body").stop().animate({
    scrollTop: offsetTop
  }, 300);

  // Bind click handler to menu items
  // so we can get a fancy scroll animation
  menuItems.click(function(e){
    var href = $(this).attr("href");
    var offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;

    $("html, body").stop().animate({
      scrollTop: offsetTop
    }, 300);
    e.preventDefault();
  });

  // Bind to scroll
  $(window).scroll(function(){
    // Get container scroll position
    var fromTop = $(this).scrollTop()+topMenuHeight;

    // Get id of current scroll item
    var cur = scrollItems.map(function(){
      if ( $(this).offset().top < fromTop ) {
        return this;
      }
    });
    // Get the id of the current element
    cur = cur[cur.length-1];
    var id = cur && cur.length ? cur[0].id : "";


    if (lastId !== id) {
      lastId = id;
      // Set/remove active class
      menuItems
        .parent().removeClass("active")
        .end().filter("[href=#"+id+"]").parent().addClass("active");

      history.pushState({}, "", "#"+id);
    }

  });
*/

})(jQuery);

(function(doc, script) {
  var js,
      fjs = doc.getElementsByTagName(script)[0],
      frag = doc.createDocumentFragment(),
      add = function(url, id) {
          if (doc.getElementById(id)) {return;}
          js = doc.createElement(script);
          js.src = url;
          id && (js.id = id);
          frag.appendChild( js );
      };

    // Google+ button
    add("http://apis.google.com/js/plusone.js");
    // Facebook SDK
    add("//connect.facebook.net/en_US/all.js#xfbml=1&appId=200103733347528", "facebook-jssdk");
    // Twitter SDK
    add("//platform.twitter.com/widgets.js");

    fjs.parentNode.insertBefore(frag, fjs);
}(document, "script"));
