<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package _mbbasetheme
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header entry-header-full">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
		<?php
  		if ( has_post_thumbnail() ) {
  		  $args = array('class' => 'full-page-img');
  			the_post_thumbnail('full-page', $args);
  		} 
		?>
	</header><!-- .entry-header -->

  <div class="wrap">  
    <div id="media-container">
      <?php
        $args=array(
          'post_type'=> 'attachment',
//          'post_mime_type' => 'image',
          'posts_per_page' => 50
        );
        $my_query = null;
        $my_query = new WP_Query($args);
        
        // The Loop
        while ( $my_query->have_posts() ) : $my_query->the_post(); 
        ?>
        <div class="item">
          <?php 
            
            the_title();
            
          ?>
        </div>
        <?php
        
        
        endwhile;
        // Reset Post Data
        wp_reset_postdata();
      ?>
    </div>    
    
  	<footer class="entry-footer">
  		<?php edit_post_link( __( 'Edit', '_mbbasetheme' ), '<span class="edit-link">', '</span>' ); ?>
  	</footer><!-- .entry-footer -->
  </div>
</article><!-- #post-## -->
