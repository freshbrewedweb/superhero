<?php
/**
 *
 * Template Name: Help Out
 *
 * @package _mbbasetheme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

  			<?php while ( have_posts() ) : the_post(); ?>
  
  				<?php get_template_part( 'content', 'fullpage' ); ?>
    
  			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
    <!-- Navigation steps must match the order and number of steps in the sections (.step) below. This excludes the li's designated for prev/next -->
    <div class="wrap">
		<?php
      //SETUP QUERY FOR STEPS  		
      $args=array(
        'post_type'         => 'page',
        'post_parent'       => $post->ID,
        'orderby'           => 'menu_order',
        'order'             => 'ASC',
        'posts_per_page'    => -1
      );
      $my_query = null;
      $my_query = new WP_Query($args);
    ?>
    <section class="helpout">
      <nav class="helpout-nav">
        <ul>
          <li class="nav prev"><i class="fa fa-chevron-left"></i>
          <?php
          // The Loop
          while ( $my_query->have_posts() ) : $my_query->the_post(); 
          ?>         
          <li><?php the_title() ?> 
          <?php
          endwhile;                  
          ?>
          <li class="nav next"><i class="fa fa-chevron-right"></i>
        </ul>
      </nav>
    
      <?php
      // The Loop
      while ( $my_query->have_posts() ) : $my_query->the_post(); 
      ?>         
      <section class="step">
        <h1><?php the_title() ?></h1>
        <?php echo get_the_content() ?>
      </section>
      <?php
      endwhile;                  
      ?>
<!--
  HARDCODED
      <section class="step">
  
        <h1>Follow <small>Stay Up to Date</small></h1>
        
        <div class="helpout-follow">
          <div class="helpout-follow-widgets">
            <h2><i class="fa fa-share-alt"></i> Social</h2>
            <ul class="helpout-follow-social">
              <li><div class="fb-like" data-href="https://www.facebook.com/valhalla.movement" data-width="80" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>      
              <li><a href="https://twitter.com/govalhalla" class="twitter-follow-button" data-show-count="false" data-lang="en">Follow @govalhalla</a>          
              <li><div class="g-follow" data-annotation="none" data-height="20" data-href="https://plus.google.com/114507753098106674242" data-rel="publisher"></div>
            </ul>          
  
            <form method="post" class="helpout-signup"> 
              <input id="email_subscribe" name="email_subscribe" type="text" placeholder="your-email@example.com" class="required"> 
              <input style="display:none;" type="checkbox" id="list_738915" name="lists[]" value="800024" checked="checked">
            	<input style="display:none;" id="volunteer_check" name="lists[]" value="837226" type="checkbox" checked="checked">
              <input id="webform_submit_button" value="Subscribe Via Email" type="submit" class="submit button" data-default-text="Subscribe Via Email" data-submitting-text="Submitting.." data-invalid-text="You forgot some required fields"> 
            </form>      
          </div>
          <div class="helpout-follow-video">
            <h2><i class="fa fa-film"></i> Video</h2>
            <h3>Youtube</h3>
            <p>Valhalla is constantly working at creating high quality content to keep you up to date with your progress, expose you to new ways of thinking, and entertain you while remaining informative.
            <p><a href="http://www.youtube.com/user/TheValhallaMovement" target="_blank" class="helpout-btn">YouTube Subscribe</a>
      
            <h3>Podcasts</h3>
            <p>Rate Comment & Subscribe to Valhalla's Superhero Academy Podcast      
            <p><a href="https://itunes.apple.com/ca/podcast/valhallas-superhero-academy/id888603342?mt=2&uo=4" class="helpout-btn">Itunes</a>
          </div>        
        </div>
        
      </section>
  
      <section class="step">
  
        <h1>Spread the Word</h1>
        <div class="helpout-spread">
          <textarea id="helpout-message">Check out @GoValhalla -- they're trying to make sustainable communal living mainstream! http://youtu.be/pg4v035zGjA</textarea>
          
          <h2>Share on</h2>
          <ul>
            <li><a href="javascript:;" class="helpout-share" data-rel="facebook"><i class="fa fa-facebook"></i> Facebook</a>
            <li><a href="javascript:;" class="helpout-share" data-rel="twitter"><i class="fa fa-twitter"></i> Twitter</a>
            <li><a href="javascript:;" class="helpout-share" data-rel="googleplus"><i class="fa fa-google-plus"></i> Google+</a>
          </ul>
        </div>
        
      </section>
  
      <section class="step">
  
        <h1>Use Our Media</h1>
        <div class="helpout-media">
          <a href="#"><span>Download Media Package</span></a>
          <p><strong>Includes:</strong> Facebook Cover Photo, Twitter Cover Photo, Google Plus Cover Photo, Logos of all shapes and sizes!
        </div>
        
      </section>
  
      <section class="step">
        
        <h1>Crowdspeaking <small>through Thunderclap</small></h1>
        
        <div class="helpout-thunderclap">
          <div class="helpout-thunderclap-info">
            <img src="assets/img/thunderclap.png" alt="thunderclap" width="442" height="74">
            <h2>What is Thunderclap?</h2>
            <p>Thunderclap is the first crowd-speaking platform that helps people be heard by saying something together.
            <h2>How does it work?</h2>
            <p>If you reach your supporter goal, Thunderclap will blast out a timed Twitter, Facebook, or Tumblr post from all your supporters, creating a wave of attention.
          </div>
          <iframe frameborder="0" height="540px" src="https://www.thunderclap.it/projects/12909-a-society-of-love-we-are/embed" width="250px"></iframe>
        </div>
          
      </section>
  
      <section class="step">
        
        <h1>Crowdsourcing <small>Greenseed</small></h1>
        <div class="helpout-greenseed">
          <div class="helpout-greenseed-info">
            <img src="assets/img/greenseed.png" alt="greenseed" width="440" height="99">
            <p>Greenseed is Valhalla's very own crowdsourcing platform which is currently in it's first Beta stage we will be opening the potential to funding your own projects on Greenseed in the near future. 
          </div>
          <iframe frameborder="0" height="540px" src="https://www.thunderclap.it/projects/12909-a-society-of-love-we-are/embed" width="250px"></iframe>
        </div>
          
      </section>
  
      <section class="step">
        
        <h1>Donate <small>and receive the solar shed plans!</small></h1>
        <div class="helpout-donate">
          <p><a href="https://www.paypal.com/ca/cgi-bin/webscr?cmd=_flow&SESSION=1UEaDkSt79vBNUQayucn0E__l2soR9gPW27SVdB_SIYpTvpL24dPoilCaLe&dispatch=5885d80a13c0db1f8e263663d3faee8d5402c249c5a2cfd4a145d37ec05e9a5e" class="helpout-btn">Donate <i class="fa fa-heart"></i></a>
          <p>Donate to help us grow and you'll receive our solar shed plans by entering your email below.
    
          <form method="post" class="helpout-signup"> 
            <input id="email_subscribe" name="email_subscribe" type="text" placeholder="your-email@example.com" class="required"> 
            <input style="display:none;" type="checkbox" id="list_738915" name="lists[]" value="800024" checked="checked">
          	<input style="display:none;" id="volunteer_check" name="lists[]" value="837226" type="checkbox" checked="checked">
            <input id="webform_submit_button" value="Subscribe Via Email" type="submit" class="submit button" data-default-text="Subscribe Via Email" data-submitting-text="Submitting.." data-invalid-text="You forgot some required fields"> 
          </form>      
        </div>
          
      </section>
-->
  
      
    </section>
    </div><!-- .wrap -->		
	</div><!-- #primary -->
</div>

<?php get_footer(); ?>
