<?php
/**
 * Template Name: Feature Page
 *
 * @package _mbbasetheme
 */

get_header();

$course_img = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'full');

?>
<div id="primary" class="content-area classroom" style="background-image:url(<?php echo $course_img[0] ?>)">
	<main id="main" class="site-main" role="main">
    <article id="post-<?php the_ID(); ?>">
      <div class="wrap">
				<?php the_content() ?>
      </div><!-- .wrap -->
    </article><!-- #post-## -->
    <?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() ) :
				comments_template();
			endif;
		?>
  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
