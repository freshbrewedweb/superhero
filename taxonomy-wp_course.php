<?php
/**
 * The Template for displaying all single posts.
 *
 * @package _mbbasetheme
 */

get_header(); ?>

<?php
	$user = wp_get_current_user();
	$user_groups = new Groups_User( $user->ID );
	$groups = array();
	foreach( $user_groups->groups as $group ) {
		$groups[] = 'uc-profile--' . strtolower($group->name);
	}
	$groups_class = implode(' ', $groups);

	$course = $wp_query->get_queried_object();
	$course_img_id = get_term_meta($course->term_id, 'image');
	$course_img = wp_get_attachment_image_src($course_img_id[0], 'full');
?>
<div id="primary" class="content-area classroom" style="background-image:url(<?php echo $course_img[0] ?>)">
	<main id="main" class="site-main" role="main">
    <article id="post-<?php the_ID(); ?>">
      <div class="wrap">
        <div class="class-nav">
          <?= apply_filters(
            'courses',
            array('class'=> 'class-breadcrumb', 'thumbnail' => false)
          ) ?>
          <div class="row">
            <div class="col-md-7">
              <?= apply_filters(
                'course_list',
                array(
                  'numbered' => 'true',
                  'orderby' => 'menu_order',
                  'class' => 'class-nav-list',
                  'count_class' => 'class-order',
                )
              ) ?>
            </div>

            <div class="col-md-5">
              <?= apply_filters('course_progress', array()) ?>

              <div class="user-class-profile<?php if($groups_class) echo ' ' . $groups_class ?>">

                <div class="user-class-avatar">
                  <?php echo get_avatar($user->user_email, 109) ?>
                  <span class="user-class-avatar__level">
                  <?php if($subscription == "Apprentice"): ?>
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-apprentice--color.png" alt="Apprentice Level Icon">
                  <?php elseif($subscription == "Hero"): ?>
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-hero--color.png" alt="Hero Level Icon">
                  <?php elseif($subscription == "Superhero"): ?>
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-superhero--color.png" alt="Superhero Level Icon">
                  <?php elseif($subscription == "Mastery"): ?>
                  <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/images/icon-mastery--color.png" alt="Mastery Level Icon">
                  <?php endif; ?>
                  </span>
                </div>

                <h2 class="user-class-profile-name"><?php echo $user->user_nicename ?></h2>

                <!-- <a href="#" class="btn btn-primary btn-lg btn-block">Take the Test</a> -->
              </div>
              <?php
              foreach(wp_get_post_terms( get_the_ID(), 'wp_course' ) as $term):
                $name = $term->name;
                $id = "course_" . $term->term_id;
                if( have_rows('level_bonus', $id) ):
              ?>
                <div class="class-bonuses">
                  <p>Level Bonuses: <strong><?= $name ?></strong></p>
                  <?php while ( have_rows('level_bonus', $id) ) : the_row(); ?>
                    <p><a href="<?php the_sub_field('button_url'); ?>" class="btn btn-light btn-lg btn-block"><?php the_sub_field('button_text'); ?></a>
                  <?php endwhile; ?>
                </div>
              <?php endif; ?>
            <?php endforeach; ?>

            </div>
          </div>
        </div><!-- .class-nav -->

      </div><!-- .wrap -->
    </article><!-- #post-## -->

    <div class="classroom-footer wrap">
      <?php dynamic_sidebar( 'classroom' ) ?>
    </div>
    <?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() ) :
				comments_template();
			endif;
		?>
  </main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
