<?php
/**
 *
 * Template Name: Scroller
 *
 * @package _mbbasetheme
 */

get_header(); ?>

<div id="content" class="site-content">

	<div id="primary" class="content-area">
	
	  <?php if( get_field("has_slider") == 1 ): ?>
	  
      <?php 
      
      $images = get_field('images');
      
      if( $images ): ?>
      <style>
      .image-slider {
        position: relative;
      }
      .image-slider img {
        width: 100%;
        height: auto;
      }
      .image-slider .slides {
        margin: 0;
        padding: 0;
      }
      .image-slider .flex-control-nav {
        position: absolute;
        width: 100%;
        text-align: center;
        padding: 0;
        bottom: 0;
      }
      .image-slider .flex-control-nav li a {
        box-shadow: 0 0 6px rgba(255,255,255,0.6);
      }
      </style>
      <div class="image-slider">
        <ul class="slides">
          <?php foreach( $images as $image ): ?>
          <li>
              <a href="<?php echo $image['url']; ?>">
                   <img src="<?php echo $image['sizes']['full-page']; ?>" alt="<?php echo $image['alt']; ?>" />
              </a>
              <!-- <p><?php echo $image['caption']; ?></p> -->
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <script>
        (function($) {
          $(window).load(function() {
            $('.image-slider').flexslider({
              animation: "slide",
              directionNav: false,
              slideshow: true,
              
            });
          });            
        })(jQuery);
        </script>            

      <?php endif; ?>
	  
	  <?php else: ?>
    <?php
      $image_url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
      $section_styles = ' style="';
      if($image_url) {
        $section_styles .= 'background-image: url('.$image_url.');';
      }
      if( get_field("text_color") ) {
        $section_styles .= 'color:'.get_field("text_color").';';
      }
      if( get_field("text_shadow_color") ) {
        $section_styles .= 'text-shadow: 2px 2px '.get_field("text_shadow_color").';';
      }
      if( get_field("background_color") ) {
        $section_styles .= 'background-color:'.get_field("background_color").';';
      }        
      $section_styles .= '"';

      $section_class = 'section';
      if( get_field("section_class") ) {
        $section_class .= " " . get_field("section_class");
      } else {
        $section_class .= " s-bigpad s-dark";
      }
      
    ?>	
		<main id="main" class="site-main <?php echo $section_class ?>" role="main"<?php echo $section_styles ?>>
    
			<?php while ( have_posts() ) : the_post(); ?>
        <div class="wrap">
  		    <?php if( get_field("no_title") != 1  ): ?>
  				<h1><?php the_title() ?></h1>
  				<?php endif; ?>
  				
  				<?php
    				if( get_field('disable_format') == 1 ) {
      				echo $post->post_content;
    				} else {
              the_content();
    				}
    				
  				?>
        </div>
			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
		<?php endif ?>
		
		<?php
  		
      $args=array(
        'post_type'         => 'page',
        'post_parent'       => $post->ID,
        'orderby'           => 'menu_order',
        'order'             => 'ASC',
        'posts_per_page'    => -1
      );
      $my_query = null;
      $my_query = new WP_Query($args);
      
      // The Loop
      while ( $my_query->have_posts() ) : $my_query->the_post(); 
        
        $image_url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
        $section_styles = ' style="';
        if($image_url) {
          $section_styles .= 'background-image: url('.$image_url.');';
        }
        if( get_field("text_color") ) {
          $section_styles .= 'color:'.get_field("text_color").';';
        }
        if( get_field("text_shadow_color") ) {
          $section_styles .= 'text-shadow: 2px 2px '.get_field("text_shadow_color").';';
        }
        if( get_field("background_color") ) {
          $section_styles .= 'background-color:'.get_field("background_color").';';
        }        
        $section_styles .= '"';


        $section_class = 'section';
        if( get_field("section_class") ) {
          $section_class .= " " . get_field("section_class");
        }
        
        $section_type = get_field("section_type");
      ?>
  		<section class="<?php echo $section_class; ?>"<?php echo $section_styles ?>>
  		  <div class="wrap">
  		    <?php if($section_type == "blogfeed"): ?>
            
            <ul class="s-blogfeed">
            <?php
              $args=array(
                'post_type'         => 'post',
                'orderby'           => 'date',
                'order'             => 'DESC',
                'meta_key'          => '_thumbnail_id',
                'posts_per_page'    => 3
              );
              $blog_query = null;
              $blog_query = new WP_Query($args);
              
              // The Loop
              while ( $blog_query->have_posts() ) : $blog_query->the_post(); 
              ?>
              	<li>
              	<?php
                	$tags = get_the_tags(get_the_id());
                	$tag_class = "";
                	foreach( $tags as $tag ) {
                  	$tag_class[] = $tag->slug;
                	}
                	$tag_class = implode(" ", $tag_class);
              	?>
              	  <a href="<?php the_permalink() ?>"<?php if( $tag_class != "" ) echo ' class="'.$tag_class.'"'; ?>>
              	    <div class="s-blogfeed-content">
                      <h3 class="h4"><?php the_title() ?></h3>
                      <span class="btn btn-light btn-small"><?php echo apply_filters('read_more', __('Read More', 'superhero') )  ?> <i class="fa fa-arrow-right"></i></span>
              	    </div>
                	  <?php the_post_thumbnail( 'square' ); ?>
              	  </a>
              <?php
              
              
              endwhile;
              // Reset Post Data
              wp_reset_postdata();
              
            ?>
            </ul>
          <?php elseif($section_type == "testimonials"): ?>
            <div class="s-testimonials">            
    		    <?php if( get_field("no_title") != 1  ): ?>
        		<h1><?php the_title() ?></h1>
        		<?php endif ?>
            <div class="flexslider">            
            <ul class="slides">
            <?php
              $args=array(
                'post_type'=> 'testimonial',
                'posts_per_page' => 3
              );
              $testimonial_query = null;
              $testimonial_query = new WP_Query($args);
              
              // The Loop
              while ( $testimonial_query->have_posts() ) : $testimonial_query->the_post(); 
              ?>
              <li>
                <figure class="s-testimonial">
                  <?php the_post_thumbnail( 'square' ); ?>
                	<blockquote>
                	  <?php the_excerpt() ?>
                	</blockquote>
                  <figcaption><?php the_title() ?></figcaption>
                </figure>
              </li>
              <?php              
              
              endwhile;
              // Reset Post Data
              wp_reset_postdata();
              
            ?>              
            </ul>
            </div>
            </div>
            <script>
            (function($) {
              $(window).load(function() {
                $('.flexslider').flexslider({
                  animation: "slide",
                  directionNav: false,
                  slideshow: true,
                  
                });
              });            
            })(jQuery);
            </script>            
            
  		    <?php else: ?>
  		    <?php if( get_field("no_title") != 1  ): ?>
      		<h1><?php the_title() ?></h1>
      		<?php endif ?>

          <?php
    				if( get_field('disable_format') == 1 ) {
      				echo $post->post_content;
    				} else {
              the_content();
    				}            
          ?>

      		<?php endif; ?>
  		  </div>
  		</section>
      <?php
      
      
      endwhile;
      // Reset Post Data
      wp_reset_postdata();
      

		?>
	</div><!-- #primary -->

</div><!-- #content -->

<?php get_footer(); ?>
