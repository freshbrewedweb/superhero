<?php
/**
 * _mbbasetheme functions and definitions
 *
 * @package _mbbasetheme
 */

/****************************************
Theme Setup
*****************************************/

/**
 * Theme initialization
 */
require get_template_directory() . '/lib/init.php';

/**
 * Custom theme functions definited in /lib/init.php
 */
require get_template_directory() . '/lib/theme-functions.php';

/**
 * Helper functions for use in other areas of the theme
 */
require get_template_directory() . '/lib/theme-helpers.php';

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/lib/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/lib/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/lib/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/lib/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/lib/inc/jetpack.php';

/**
 * Load Custom Post Type
 */
require get_template_directory() . '/lib/custom-post-type.php';

/**
 * WooCommerce
 */
require get_template_directory() . '/lib/woocommerce.php';


/****************************************
Require Plugins
*****************************************/

require get_template_directory() . '/lib/class-tgm-plugin-activation.php';
require get_template_directory() . '/lib/theme-require-plugins.php';

// add_action( 'tgmpa_register', 'mb_register_required_plugins' );


/****************************************
Misc Theme Functions
*****************************************/


/**
* Define copyright link
*/
function ubiweb_copyright() {
	$theme = wp_get_theme();
	$url = $theme->get( 'AuthorURI' );
	$name = get_site_option( 'site_name' );
	$html = '<a href="' . $url . '">' . $name . '</a>';
	return $html;
}

/**
 * Define custom post type capabilities for use with Members
 */
add_action( 'admin_init', 'mb_add_post_type_caps' );
function mb_add_post_type_caps() {
	// mb_add_capabilities( 'portfolio' );
}

/**
 * Filter Yoast SEO Metabox Priority
 */
add_filter( 'wpseo_metabox_prio', 'mb_filter_yoast_seo_metabox' );
function mb_filter_yoast_seo_metabox() {
	return 'low';
}

// function fb_move_admin_bar() {
//     echo '
//     <style type="text/css">
//     body.admin-bar #footer {
//        padding-bottom: 28px;
//     }
//     #wpadminbar {
//         top: auto !important;
//         bottom: 0;
//     }
//     #wpadminbar .quicklinks .menupop ul {
//         bottom: 28px;
//     }
//     </style>';
// }
// // on frontend area
// add_action( 'wp_head', 'fb_move_admin_bar' );

//add_theme_support( 'admin-bar', array( 'callback' => '__return_false') );
// show admin bar only for admins
if (!current_user_can('manage_options')) {
	add_filter('show_admin_bar', '__return_false');
}
// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_false');
}

/*********************
IMAGE SIZES
*********************/
add_image_size('square', 500, 500, true);
add_image_size('full-content', 750, 350, true);
add_image_size('full-page', 1600, 400, true);

// Replaces the excerpt "more" text by a link
if( !function_exists('new_excerpt_more') ) {
function new_excerpt_more($more) {
       global $post;
	return '...<p><a class="btn" href="'. get_permalink($post->ID) . '"> '. __('Read the full article') .' <i class="fa fa-arrow-right"></i></a>';
}
add_filter('excerpt_more', 'new_excerpt_more');
}

function has_children($id) {
  $pages = get_pages('child_of=' . $id);
  return count($pages);
}

//CUSTOM LOGIN CSS
function asgard_login_css() {
	$bg = get_option('background');
	$bg_url = $bg['upload'];
	echo '<style>
	body {
		background: url('.$bg_url.') no-repeat center center fixed;
		background-size: cover;
	}
	</style>';
}
add_action('login_head', 'asgard_login_css');


//ADD STUFF TO HEAD
function asgard_custom_head_scripts(){
	global $post;
	$html = get_post_meta($post->ID, 'wp_head', TRUE);
	if( $html ) echo $html;
}
add_action('wp_head', 'asgard_custom_head_scripts');

//WooCommerce Support
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// 3. Hide ACF field group menu item
//add_filter('acf/settings/show_admin', '__return_false');

// Mastery = 2
// Apprentice = 1
// Novice = 3
// Mastery + Coaching = 6
// Hero = 8
// Superhero = 9

// Groups:
// Apprentice = 2
// Hero = 3
// Mastery = 4
// Superhero = 5
// Mastery with Coaching = 6
// Novice = 7

// $members = get_users( array(
// 	'meta_key' => 'rcp_subscription_level',
// 	'meta_value' => 2
// ) );
// // Array of WP_User objects.
// foreach ( $members as $user ) {
// 	global $wpdb;
// 	$data = array(
// 		'user_id' => $user->ID,
// 		'group_id' => 4,
// 	);
// 	$result = $wpdb->insert( 'wp_groups_user_group', $data );
// 	var_dump($result);
// }
// die();
