<?php
/**
 * Template Name: Classroom Salespage
 *
 *
 * @package _mbbasetheme
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
<?php $feat_image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
<div id="primary" class="content-area classroom" style="background-image:url(<?php echo $feat_image ?>)">
	<main id="main" class="site-main" role="main">
    <article id="post-<?php the_ID(); ?>">
      <div class="wrap">
        <div class="class-header">
          <h1 class="class-title"><?php the_title(); ?></h1>
        </div>
        <div class="class-content">
          <div class="class-video fitvid">
            <?php the_content() ?>
          </div>
        </div><!-- .class-content -->
        <div class="class-nav">
					<?php the_field('classes') ?>
        </div><!-- .class-nav -->

      </div><!-- .wrap -->
    </article><!-- #post-## -->

    <div class="classroom-footer wrap">
      <?php dynamic_sidebar( 'classroom' ) ?>
    </div>
    <?php
			// If comments are open or we have at least one comment, load up the comment template
			if ( comments_open() || '0' != get_comments_number() ) :
				comments_template();
			endif;
		?>
  </main><!-- #main -->
</div><!-- #primary -->
<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>
