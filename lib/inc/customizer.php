<?php
/**
 * _mbbasetheme Theme Customizer
 *
 * @package _mbbasetheme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function _mbbasetheme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
}
add_action( 'customize_register', '_mbbasetheme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function _mbbasetheme_customize_preview_js() {
	wp_enqueue_script( '_mbbasetheme_customizer', get_template_directory_uri() . 'assets/js/vendor/customizer.js', array( 'customize-preview' ), '20130508', true );
}
add_action( 'customize_preview_init', '_mbbasetheme_customize_preview_js' );

//Theme Customization
add_action( 'customize_register', 'jtd_customize_register' );
function jtd_customize_register($wp_customize) {

  //Logo
  $wp_customize->add_section( 'sitelogo', array(
    'title' => 'Site Logo', // The title of section
    'description' => 'Settings for the Site Logo', // The description of section
  ) );

  $wp_customize->add_setting( 'sitelogo[upload]', array(
      'type' => 'option',
  ) );

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'sitelogo[upload]', array(
      'label'   => 'Upload',
      'section' => 'sitelogo',
  ) ) );

  //Favicon
  $wp_customize->add_section( 'favicon', array(
          'title' => 'Favicon', // The title of section
          'description' => 'Upload a favicon', // The description of section
  ) );

  $wp_customize->add_setting( 'favicon[uploadpng]', array(
      'type' => 'option',
  ) );

  $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'favicon[uploadpng]', array(
      'label'   => 'Upload PNG',
      'section' => 'favicon',
  ) ) );

	//Favicon
	$wp_customize->add_section( 'login', array(
		'title' => 'Login', // The title of section
		'description' => 'Login page options', // The description of section
	) );

	$wp_customize->add_setting( 'background[upload]', array(
		'type' => 'option',
	) );

	$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'background[upload]', array(
		'label'   => 'Upload PNG',
		'section' => 'login',
	) ) );

	$wp_customize->add_setting( 'title_tagline[icon]', array(
      'type' => 'option',
  ) );

  $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'title_tagline[icon]', array(
      'label'   => 'CTA Icon',
      'section' => 'title_tagline',
  ) ) );

}
