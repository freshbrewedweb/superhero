<?php
/**
 * _mbbasetheme theme functions definted in /lib/init.php
 *
 * @package _mbbasetheme
 */


/**
 * Register Widget Areas
 */
function mb_widgets_init() {
	// Main Sidebar
	register_sidebar( array(
		'name'          => __( 'Sidebar', '_mbbasetheme' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	// Kick Sidebar
	register_sidebar( array(
		'name'          => __( 'Kick Sidebar', '_mbbasetheme' ),
		'id'            => 'sidebar-kick',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="kick-widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );

	// Footer
	register_sidebar( array(
		'name'          => __( 'Footer', '_mbbasetheme' ),
		'id'            => 'footer',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="foot-cell %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="foot-cell-title">',
		'after_title'   => '</h1>',
	) );

	// Classroom
	register_sidebar( array(
		'name'          => __( 'Classroom Footer', 'superhero' ),
		'id'            => 'classroom',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="class-footer__cell %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="class-footer__title">',
		'after_title'   => '</h1>',
	) );

}

/**
 * Remove Dashboard Meta Boxes
 */
function mb_remove_dashboard_widgets() {
	global $wp_meta_boxes;
	// unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}

/**
 * Change Admin Menu Order
 */
function mb_custom_menu_order( $menu_ord ) {
	if ( !$menu_ord ) return true;
	return array(
		// 'index.php', // Dashboard
		// 'separator1', // First separator
		// 'edit.php?post_type=page', // Pages
		// 'edit.php', // Posts
		// 'upload.php', // Media
		// 'gf_edit_forms', // Gravity Forms
		// 'genesis', // Genesis
		// 'edit-comments.php', // Comments
		// 'separator2', // Second separator
		// 'themes.php', // Appearance
		// 'plugins.php', // Plugins
		// 'users.php', // Users
		// 'tools.php', // Tools
		// 'options-general.php', // Settings
		// 'separator-last', // Last separator
	);
}

/**
 * Hide Admin Areas that are not used
 */
function mb_remove_menu_pages() {
	// remove_menu_page( 'link-manager.php' );
}

/**
 * Remove default link for images
 */
function mb_imagelink_setup() {
	$image_set = get_option( 'image_default_link_type' );
	if ( $image_set !== 'none' ) {
		update_option( 'image_default_link_type', 'none' );
	}
}

/**
 * Enqueue scripts
 */
function mb_scripts() {
	wp_enqueue_style( '_mbbasetheme-style', get_stylesheet_directory_uri() . '/assets/styles/build/core.css' );
	wp_enqueue_style( 'rrssb', get_stylesheet_directory_uri() . '/assets/js/vendor/RRSSB/css/rrssb.css' );
	wp_enqueue_style( 'featherlight', get_stylesheet_directory_uri() . '/assets/js/vendor/featherlight/src/featherlight.css' );

	//wp_enqueue_script( '_mbbasetheme-navigation', get_template_directory_uri() . 'assets/js/vendor/navigation.js', array(), '20120206', true );

	wp_enqueue_script( '_mbbasetheme-skip-link-focus-fix', get_stylesheet_directory_uri() . '/assets/js/vendor/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'ubi-waypoints', get_stylesheet_directory_uri() . '/assets/js/vendor/jquery-waypoints/waypoints.min.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'ubi-waypoints-sticky', get_stylesheet_directory_uri() . '/assets/js/vendor/jquery-waypoints/shortcuts/sticky-elements/waypoints-sticky.min.js', array('jquery','ubi-waypoints'), '20130115', true );

	wp_enqueue_script( 'ubi-featherlight', get_stylesheet_directory_uri() . '/assets/js/vendor/featherlight/src/featherlight.js', array('jquery'), '20130115', true );
	wp_enqueue_script( 'ubi-fitvids', get_stylesheet_directory_uri() . '/assets/js/vendor/fitvids/jquery.fitvids.js', array('jquery'), '20130115', true );

	wp_register_script( 'ubi_helpout', get_stylesheet_directory_uri() . '/assets/js/helpout.js', array('jquery'), '20130115', true );
	wp_register_script( 'ubi-isotope', get_stylesheet_directory_uri() . '/assets/js/vendor/isotope/dist/isotope.pkgd.min.js', array(), '20141001', true );
	wp_register_script( 'ubi-infinite-scroll', get_stylesheet_directory_uri() . '/assets/js/vendor/jquery-infinite-scroll/jquery.infinitescroll.min.js', array('jquery'), '20141001', true );
	wp_register_script( 'ubi-flexslider', get_stylesheet_directory_uri() . '/assets/js/vendor/flexslider/jquery.flexslider-min.js', array('jquery'), '20141001', true );
	wp_register_script( 'ubi-rrssb', get_stylesheet_directory_uri() . '/assets/js/vendor/RRSSB/js/rrssb.min.js', array('jquery'), '20141001', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if( is_singular() ) {
		wp_enqueue_script( 'ubi-rrssb' );
	}

	if ( is_page_template('page-helpout.php') ) {
		wp_enqueue_script( 'ubi_helpout' );
	}

	if ( is_page_template('page-scroller.php') ) {
		wp_enqueue_script( 'ubi-flexslider' );
	}

	if ( is_page_template('page-media.php') ) {
		wp_enqueue_script( 'ubi-isotope' );
		wp_enqueue_script( 'ubi-infinite-scroll' );
	}


	if ( !is_admin() ) {
		wp_enqueue_script( 'jquery' );
		wp_enqueue_script( 'customplugins', get_stylesheet_directory_uri() . '/assets/js/plugins.min.js', array('jquery'), NULL, true );
		wp_enqueue_script( 'customscripts', get_stylesheet_directory_uri() . '/assets/js/main.min.js', array('jquery'), NULL, true );
	}

}

/**
 * Remove Query Strings From Static Resources
 */
function mb_remove_script_version( $src ){
	$parts = explode( '?ver', $src );
	return $parts[0];
}

/**
 * Remove Read More Jump
 */
function mb_remove_more_jump_link( $link ) {
	$offset = strpos( $link, '#more-' );
	if ($offset) {
		$end = strpos( $link, '"',$offset );
	}
	if ($end) {
		$link = substr_replace( $link, '', $offset, $end-$offset );
	}
	return $link;
}


function ubi_get_post_formats(){

  $pf_array = array();

  if ( current_theme_supports( 'post-formats' ) ) {
      $post_formats = get_theme_support( 'post-formats' );

      if ( is_array( $post_formats[0] ) ) {
          foreach( $post_formats[0] as $post_format ) {
            $args = array(
              'tax_query' => array(
            		array(
            			'taxonomy' => 'post_format',
            			'field'    => 'slug',
            			'terms'    => array( 'post-format-' . $post_format ),
            		),
              )
            );
            $the_query = new WP_Query( $args );
            $post_format_count = $the_query->found_posts;

            $pf_array[$post_format] = $post_format_count;

          }

      }
  }


/*
*/

  return $pf_array;

}


function sa_testimonial_slider( $atts ) {
	// Attributes
	extract( shortcode_atts(
		array(
			'ids' => '',
		), $atts )
	);
	wp_enqueue_script( 'ubi-flexslider' );

$html = '
<div class="m-testimonials">
	<div class="flexslider">
		<ul class="slides">';

			$args=array(
				'post_type'=> 'testimonial',
				'post__in' => explode(',', $atts['ids'])
			);
			$testimonial_query = null;
			$testimonial_query = new WP_Query($args);

			// The Loop
			while ( $testimonial_query->have_posts() ) : $testimonial_query->the_post();
$html .= '
			<li>
				<figure class="s-testimonial">' . get_the_post_thumbnail( $post->ID, 'square' ) . '
					<blockquote>' . get_the_excerpt() . '</blockquote>
					<figcaption>' . get_the_title() . '</figcaption>
				</figure>
			</li>';

			endwhile;
			// Reset Post Data
			wp_reset_postdata();

$html .= '
		</ul>
	</div>
</div>
<script>
(function($) {
	$(window).load(function() {
		$(".flexslider").flexslider({
			animation: "slide",
			directionNav: false,
			slideshow: true,

		});
	});
})(jQuery);
</script>';
 return $html;
}

add_shortcode( 'testimonials', 'sa_testimonial_slider' );

add_action('get_header', 'my_filter_head');
function my_filter_head() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}
