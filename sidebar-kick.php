<?php
/**
 * The Sidebar containing the main widget areas.
 *
 * @package _mbbasetheme
 */
?>
	<div id="kick-sidebar" class="kick-sidebar" role="complementary">
	  <button id="kickClose" class="kick-close"><i class="fa fa-times-circle"></i></button>
		<?php if ( ! dynamic_sidebar( 'sidebar-kick' ) ) : ?>

			<aside id="search" class="widget widget_search">
				<?php get_search_form(); ?>
			</aside>

			<aside id="archives" class="widget">
				<h1 class="widget-title"><?php _e( 'Archives', '_mbbasetheme' ); ?></h1>
				<ul>
					<?php wp_get_archives( array( 'type' => 'monthly' ) ); ?>
				</ul>
			</aside>

			<aside id="meta" class="widget">
				<h1 class="widget-title"><?php _e( 'Meta', '_mbbasetheme' ); ?></h1>
				<ul>
					<?php wp_register(); ?>
					<li><?php wp_loginout(); ?></li>
					<?php wp_meta(); ?>
				</ul>
			</aside>

		<?php endif; // end sidebar widget area ?>
	</div><!-- #secondary -->
  <div class="darken-page"></div>
