<?php
/**
 * Template Name: Media Grid
 *
 *
 * @package _mbbasetheme
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

      <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <div class="wrap">
        	<header class="entry-header text-center">
        		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
            <div id="filters" class="media-grid-filters">
              <button class="pill active" data-filter="*">show all</button>
          		<?php
/*
            		$post_formats = ubi_get_post_formats();
                foreach( $post_formats as $key=>$val ) {
                  echo count($val);
                  if( count($val) > 0 ) {
                    echo '<button class="pill" data-filter=".'.$key.'">'.$key.'</button>';
                  }
                }
*/
          		?>
          		<?php            		
            		$filter_cats = get_field('filter_categories');
            		foreach( $filter_cats as $cat ) {
                  echo '<button class="pill" data-filter=".cat-'.$cat->term_id.'">'. $cat->name .'</button>';	
            		}            		

            		$filter_tags = get_field('filter_tags');
            		foreach( $filter_tags as $tag ) {
                  echo '<button class="pill" data-filter=".tag-'.$tag->term_id.'">'. $tag->name .'</button>';	
            		}            		
          		?>
            </div>            
        	</header><!-- .entry-header -->
        
          <div id="media-container">
            <?php
              // Define custom query parameters
              $custom_query_args = array( 
                'posts_per_page' => 20
              );
              
              // Get current page and append to custom query parameters array
              $custom_query_args['paged'] = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
              
              // Instantiate custom query
              $custom_query = new WP_Query( $custom_query_args );
              
              // Pagination fix
              $temp_query = $wp_query;
              $wp_query   = NULL;
              $wp_query   = $custom_query;
              
              // Output custom query loop
              if ( $custom_query->have_posts() ) :
                  while ( $custom_query->have_posts() ) :
                      $custom_query->the_post();
                      
                      $filter_class = "";
                      
                      //ADD POST FORMAT CLASSES
                      if( $format = get_post_format() ) {
                        $filter_class = $format;
                      } else {
                        $filter_class = "standard";                        
                      }

                      //ADD CATEGORY CLASSES
                      $cats = wp_get_post_categories( get_the_ID() );
                      $cat_ids = array();
                      foreach($cats as $cat) {
                        $cat_ids[] = "cat-$cat";
                      }
                      $filter_class .= " ";
                      $filter_class .= implode( ' ', $cat_ids);

                      //ADD TAG CLASSES
                      $tags = wp_get_post_tags( get_the_ID() );
                      $tag_ids = array();
                      foreach($tags as $tag) {
                        $tag_ids[] = "tag-{$tag->term_id}";
                      }
                      $filter_class .= " ";
                      $filter_class .= implode( ' ', $tag_ids);
                      ?>
                      <a href="<?php the_permalink() ?>" class="media-item <?php echo $filter_class ?>">
                        
                        <div class="media-item-text<?php if(has_post_thumbnail()) echo " media-item-has-image"; ?>">
                          <h3><?php the_title() ?></h3>
                        </div>
                        <?php the_post_thumbnail('full') ?>
                      </a>                      
                      <?php
                  endwhile;
              endif;
              // Reset postdata
              wp_reset_postdata();
              
              ?>
          </div>    
          
        	<footer class="entry-footer">
        	  <div class="pagination">
            <?php

              echo '<span class="page-next pill">';
              next_posts_link( 'Newer Posts', $custom_query->max_num_pages );
              echo '</span>';
              
              // Reset main query object
              $wp_query = NULL;
              $wp_query = $temp_query;              
                          
            ?>          	  
        	  </div>
        		<?php edit_post_link( __( 'Edit', '_mbbasetheme' ), '<span class="edit-link">', '</span>' ); ?>
        	</footer><!-- .entry-footer -->          
        </div>
      </div><!-- #post-## -->

		</main><!-- #main -->
	</div><!-- #primary -->


  <script>
  (function($) {
    $( window ).load(function() {
      var $container = $('#media-container');
      // init
      $container.isotope({
        // options
        itemSelector: '.media-item',
        columnWidth: 200
      });    

      // filter items on button click
      $('#filters').on( 'click', 'button', function() {
        $(this).siblings().removeClass("active");
        $(this).addClass("active");
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });
      });

      $container.infinitescroll({
        navSelector  : '.pagination',     // selector for the paged navigation 
        nextSelector : '.page-next a',  // selector for the NEXT link (to page 2)
        itemSelector : '.media-item',         // selector for all items you'll retrieve
        loading: {
            finishedMsg: 'No more pages to load.',
            img: 'http://i.imgur.com/qkKy8.gif'
          }
        },
        // call Isotope as a callback
        function( newElements ) {
          $container.isotope( 'appended', $( newElements ) ); 
        }
      );

    });    
  
  })(jQuery);  
  </script>

<?php get_footer(); ?>
