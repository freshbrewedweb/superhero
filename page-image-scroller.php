<?php
/**
 *
 * Template Name: Plain Image Scroller
 *
 * @package _mbbasetheme
 */

get_header(); ?>

<div id="content" class="site-content">

    <?php while ( have_posts() ) : the_post(); ?>
      <section class="section-image-only">    
  		  <?php the_post_thumbnail('full') ?>      
      </section>
  	<?php endwhile; // end of the loop. ?>


		<?php
  		
      $args=array(
        'post_type'         => 'page',
        'post_parent'       => $post->ID,
        'orderby'           => 'menu_order',
        'order'             => 'ASC',
        'posts_per_page'    => -1
      );
      $my_query = null;
      $my_query = new WP_Query($args);
      
      // The Loop
      while ( $my_query->have_posts() ) : $my_query->the_post(); 
        
      ?>
  		<section class="section-image-only">
  		  <?php the_post_thumbnail('full') ?>
  		</section>
      <?php
      
      
      endwhile;
      // Reset Post Data
      wp_reset_postdata();
      

		?>
	</div><!-- #primary -->

</div><!-- #content -->

<?php get_footer(); ?>
